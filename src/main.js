import { createApp } from 'vue';
import { createStore } from 'vuex';

import App from './App.vue';

const store = createStore({
    state() {
        return {
            groceries: [{ name: 'Banaan', quantity: 0, cost: 2 }, { name: 'Appel', quantity: 0, cost: 4 }],
        };
    },
    getters: {
        getGrocery(state) {
            return state.groceries;
        },
        totalAll: function (state) {
            let sum = 0;
            state.groceries.forEach(e => {
                sum += e.cost * e.quantity;
            });
            return sum;
        },
    },
    mutations: {

    },
    actions: {

    }
});
const app = createApp(App);
app.use(store);
app.mount('#app');



